class book {
  constructor(cover, pages) {
    this.cover = cover
    this.pages = pages
  }

  details() {
    return `${this.cover} has ${this.pages} pages`
  }
}

class ebook extends book {
  constructor(cover, pages, data) {
    super(cover, pages)
    this.data = data
  }

  moreDetails() {
    const bookInfo = this.details()
    return `${bookInfo} and is ${this.data} big`
  }
}

//console.log(book())

const totalRecall = new book('Total recall', 299)

console.log(totalRecall)
console.log(totalRecall.details())

const recall = new ebook('Total recall', 299, '4kb')

console.log(recall.moreDetails())
