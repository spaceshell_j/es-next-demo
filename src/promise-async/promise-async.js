// Asynchronous code

const timer = new Promise((res, rej) => {
  setTimeout(() => res("."), 100)
})

const lotsOfTime = [...Array(5000)].map(t => timer)

const timer1 = new Promise((res, rej) => {
  Promise.all(lotsOfTime).then(dot => res(dot.join('')))
})

const timer2 = new Promise((res, rej) => {
  setTimeout(() => res("2, Success!"), 2000)
})

const timer3 = new Promise((res, rej) => {
  setTimeout(() => res("3, Success!"), 4000)
})

const timer4 = new Promise((res, rej) => {
  setTimeout(() => res("4, Success!"), 5000)
})

timer1.then(time => {console.log(time); console.log(time.length)})
timer2.then(time => console.log(time))
timer3.then(time => console.log(time))
timer4.then(time => console.log(time))
console.log(timer4)

// Async await

const getDots = async () => {
  const first = await timer2
  const second = await timer3
  const third = await timer1
  return `${first} ${second} ${third}`
}

console.log(getDots())
